# OpenFlexure Brand and Identity Resources

<img src="https://gitlab.com/openflexure/openflexure-identity/raw/master/logo_all_curves.svg">


## Quick reference
* "OpenFlexure Pink" is our main colour, #C5247F or rgb(197,36,127)
* The main logo uses the [Open Sans font](https://fonts.google.com/specimen/Open+Sans)
* If you do not have Open Sans installed you should use svgs those labelled "_curves", as these have text converted into paths.


## Typography

**Primary typeface**

Open Sans is the primary typeface for OpenFlexure.

If Open Sans isn't available, Roboto is our secondary typeface.

All variations of Open Sans can be used in written text. These are:

![](README_assets/opensans_styles.png)

The OpenFlexure Logo uses only lowercase letter in Bold, Semi Bold and Light variations in the following style:

![](README_assets/LogoTypography.png)

## Colours

These are the brand colours; any application should use this palette. 

### Primary Colour

Our primary colour is **#C5247F**, also known as ‘OpenFlexure Pink’. This is the colour of the OpenFlexure emblem. This vibrant colour is our core asset, and we use it across all our brand assets.

### Secondary Colour

**#2A2A2A** is the near-black colour that should be used alongside the primary colour, and is the main colour for any text associated with the OpenFlexure brand.

### Essential Colours

**#E7E7E7** and **#FFFFFF** can be used to provide strong contrast against dark or bold backgrounds. These colours can be used for the logo and text when necessary.


## OpenFlexure Logos

All official versions of our logo can be found in this repository. The logos should not have their aspect ratios changed, their orientation or colour altered, or additional text added.

Different colour logos depending on situation are available above. Our main logos for usage are:

The primary logo (file `logo_color.svg`) for the OpenFlexure Microscope is:

![](logo_colour.svg){width=450}

The primary embelem  (file `emblem_pink.svg`) for the OpenFlexure is:

![](emblem_pink.svg){width=200}


### Logo Positioning

The OpenFlexure logo should appear in the top left or right hand corner of documents and graphics.

There are a limited number of exceptions:

* In some printed material the logo may appear at the top middle of the asset, for example in printed advertisements and event banners.
* The logo can appear in the bottom left or right-hand corner of a social media asset, where it's appropriate to feature the text at the top of the asset where it can be instantly read.

The OpenFlexure emblem should always appear on the left hand side of any logo text.


A minimum amount of clear space is required around the logo to ensure the OpenFlexure logo has high visibility and impact.

It's essential that a clear space is maintained between the OpenFlexure logo and other graphic elements such as logos, type or images, and that the logo remains unobstructed.

### Colour Variants

The primary colour for our logo is OpenFlexure pink with black text. The white or grey variations can be used on dark backgrounds and the black variation can be used on colourful backgrounds where the contrast with the pink logo is low. 

A full list of complementary colours, tints and shades for OpenFlexure pink can be found [here](https://www.colorhexa.com/c5247f).

![](colour-variants.png)

## Brand Voice

Our brand voice defines the distinct personality, tone, and style in which we communicate. It ensures consistency across all touchpoints, reinforcing our identity and building a strong connection with our audience.

**Our voice is:**

**Innovative** – OpenFlexure is at the forefront of open-source scientific hardware, pushing boundaries in accessible microscopy. Our voice should convey forward-thinking, problem-solving, and a commitment to cutting-edge research.

**Collaborative** – The project thrives on a global community of researchers, engineers, and makers. Our communication should feel welcoming, inclusive, and encouraging, fostering a sense of shared progress.

**Accessible** – A core aim of OpenFlexure is to make high-quality scientific tools available to all. Our voice should be clear, jargon-free (where possible), and inviting, ensuring that people from diverse backgrounds can engage with and contribute to the project.

We adapt our tone based on context while maintaining our core personality. Whether we are engaging on social media, drafting professional communications, or creating marketing materials, our brand voice remains authentic, recognisable, and aligned with our values.

### Project and Microscope Naming

* Use **"OpenFlexure Microscope"**, **"our microscope"**, or similar wherever possible.
* "OpenFlexure" refers to the project, not the microscope.
* The abbreviation **OFM** should only be used when absolutely necessary and must be double-checked by another team member.
* When referring to other aspects of the OpenFlexure project, such as the **Delta Stage** or **Block Stage**, ensure clarity by specifying the project name rather than just OpenFlexure.
* The names of OpenFlexure projects, such as the Microscope, Delta Stage and Block Stage, should be capitalised.
* Ensure that **OpenFlexure** is always capitalised correctly, except when referring to the website: **openflexure.org**.
* OpenFlexure is singular, i.e. OpenFlexure is an it not a they, e.g. OpenFlexure is releasing, not OpenFlexure are releasing.
* When you speak on behalf of OpenFlexure, use the plural pronouns we, us and our, e.g. We are proud to release version 7.0.

### Key Messaging and Values

* **"Low cost" is not the primary selling point** for researchers or healthcare professionals.
* Instead, highlight that the OpenFlexure Microscope is:
  * Accessible
  * Locally manufacturable
  * Maintainable by the user
  * Customisable
  * High performance
  * Robotic
  * Lab Grade
* Our mission statement is "Microscopy for everyone."

### Language and Terminology

* Use "collaborators" or "partners" instead of language that implies direct employment relationships.
* When discussing our work in the Global South, avoid terms such as:
  * "Third-world country"
  * "Poor"
  * "Underdeveloped"
* Instead, use:
  * **"Underserved"**
  * **"Underfunded"**
  * **"Low-resource"**
* We say that the microscope can be used to **study or image** diseases such as cancer and malaria. We do not say that it is used for **diagnosis**. This is not legally correct until manufacturers have  created a certified diagnostic product.
* We shouldn’t say that we are aiming for medical certification, as a design cannot be certified. Instead be clear that we are **supporting manufacturers to achieve medical certification**.

### Grammar and Style Consistency

* The distinction between **"open source"** and **"open-source"**:
  * Use **"open source"** when referring to the general concept (e.g., "The OpenFlexure Microscope is open source.")
  * Use **"open-source"** when it functions as a compound adjective (e.g., "We developed an open-source microscope.")

This guide ensures a consistent and professional voice across all communications for the OpenFlexure project. Always check usage with the team when in doubt.




